# Proyecto final

Actividades:

* Explicación del módulo `images.py` y de los ejemplos `ejemplo1.py` y `ejemplo2.py`

**Ejercicio:** "Separa una imagen en tres, según colores":

* Crea un programa que lea un nombre de fichero de la línea de comandos, y escriba tres ficheros, cada uno con un canal de color (rojo, verde, azul) de la imagen original.

* Si el fichero original tiene el nombre `fichero.ext`, los ficheros resultantes tendrán los nombres `fichero_rojo.ext`, `fichero_verde.ext` y `fichero_azul.ext`.

* El fichero se ejecutará de la siguiente forma:

```python
python3 separa.py <fichero>
```

**Ejercicio a entregar:** Proyecto final

  * **Fecha de entrega:** 17 de enero de 2024, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/final/
  * [Enunciado](final/README.md).
