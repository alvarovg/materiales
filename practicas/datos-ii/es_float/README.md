## Conversión de cadenas a float

Desarrolle un programa que contenga una función (`es_float`) que reciba como parámetro una cadena (string) y devuelva True si puede ser convertida a float (número real), False si no.
