'''
Práctica de Diccionario
'''

# sys.argv debería tener:  key_tipo_archivo  valor_archivo valor_modificar key_tipo_archivo_eliminar
import sys

def main():
    file_counts = {"jpg": 10, "txt": 14, "cvs": 4, "py": 3}
    print("DICCIONARIO")
    print(file_counts)
    #dic1 = sys.argv[1:]
    #añadir datos al diccionario
    file_counts.setdefault(sys.argv[1],sys.argv[2])
    print("AÑADIENDO VALOR AL DICCIONARIO")
    print(file_counts)
    # modificar datos de una key del diccionario
    a = sys.argv[3]
    file_counts[a]=sys.argv[4]
    print("MODIFICANDO VALOR AL DICCIONARIO")
    print(file_counts)
    #eliminar datos del diccionario
    file_counts.pop(sys.argv[5])
    print("ELIMINAR VALOR AL DICCIONARIO")
    print(file_counts)
    total = sum(file_counts[x] for x in file_counts)
    print(f"El total de ficheros es: {total}")
    print(sum(file_counts.values()))
    print("Los valores del diccionario son: ")
    for values in file_counts.values():
        print(values)

if __name__ == '__main__':
    main()
