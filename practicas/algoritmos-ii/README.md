# Algoritmos II

## Búsqueda

Actividades:

* Juego: adivina un número del 1 al 100
* Presentación: el problema de la búsqueda en listas ordenadas.

Ejercicios:

**Ejercicio:** Búsqueda binaria en lista ordenada de números.
  * Ejercicio realizado en clase
  * [Enunciado](busqueda_numeros/README.md)

**Ejercicio:** Búsqueda binaria en cadena de caracteres ordenada
  * Ejercicio realizado en clase
  * [Enunciado](busqueda_caracteres/README.md)

**Ejercicio:** Búsqueda binaria en cadena de caracteres sin ordenar
  * Ejercicio realizado en clase
  * [Enunciado](busqueda_caracteres_sin/README.md)

**Ejercicio:** Búsqueda binaria en cadena de caracteres sin ordenar, en dos ficheros
  * Ejercicio recomendado
  * [Enunciado](busqueda_caracteres_sin2/README.md)

**Ejercicio a Entregar:** Búsqueda de palabras
  * **Fecha de entrega:** 06 de diciembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/buscapalabras/
  * [Enunciado](buscapalabras/README.md)


**Referencias:**

* [Binary search (Wikipedia)](https://en.wikipedia.org/wiki/Binary_search_algorithm)
* [How to do a binary search in Python](https://realpython.com/binary-search-python/#implementing-binary-search-in-python)
* [Visualizing searching in a sorted list](https://www.cs.usfca.edu/~galles/visualization/Search.html)
