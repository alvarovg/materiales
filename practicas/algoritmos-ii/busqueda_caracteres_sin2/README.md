### Búsqueda binaria en cadena de caracteres sin ordenar, en dos ficheros

Escribe un programa que se comporte exactamente como el descrito en el ejercicio "Búsquda binaria en cadena de caracteres sin ordenar", pero en dos ficheros:

* Uno de ellos tendrá una función, `sort_string()`, que aceptará como argumento una cadena de caracteres (string) y devolverá una cadena de caracteres con los mismos caracteres, pero ordenados. Si es preciso, tendrá también las funciones auxiliares que precise.

* El otro tendrá el programa principal, en la función `main()`, que se encargará de leer los argumentos de la línea de comandos, llamar a `sort_string()` para ordenar la cadena, y luego buscar el carácter pasado como parámetro en esa cadena ordenada.

<!--Solución: [searchchars3.py](searchchars3.py) y [sortchars.py](sortchars.py)-->
