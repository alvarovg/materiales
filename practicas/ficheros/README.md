# Gestión de Fecheros

Actividades:

* Presentación de transparencias del tema "Gestión de Ficheros"

* Ejercicios:

**Ejercicio:** Leyendo y escribiendo un archivo

  * Ejercicio realizado en clase
  * [Enunciado](reading_writing_files/README.md)

 <!--
**Ejercicio:** Conversión a float

  * Ejercicio realizado en clase
  * [Enunciado](es_float/README.md)



 **Ejercicio a entregar:** Clasificador de cadenas de texto

  * **Fecha de entrega:** 13 de noviembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/clasificador/
  * [Enunciado](clasificador/README.md)
  * [Solución](clasificador/clasificador.py)
-->