
# Ejercicio: Leer y escribir en archivos

## En este ejercicio, realizaremos una práctica de lo aprendido sobre lectura y escritura de archivos jugando con algunos archivos de texto.

 Vamos a tener un archivo de texto que podremos llamar guests.txt , el que nos servirá para trabajar suponiento que almacenamos nombres de visitantes de un hotel, y medientes el uso de listas nos apoyaremos para leer y escribir en nuestro fichero.

Contendrá las siguientes funciones:

- Función agregar o cargar archivo con húespedes
- Función para imprimir el archivo
- Función que agrega más huéspedes al archivo
- Función elimina huésped que han salido
- Función Chequear huésped.

