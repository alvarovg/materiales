import sys

def print_lista(numbers: list):
    for number in numbers:
        print(number, end=" ")
    print()

def QuickSort(numbers: list) -> list:
    
    if len(numbers) < 2:
        return numbers
    else:
        pivot: int = numbers[0]
        low: list = []
        high: list = []
        
        for number in numbers[1:]:
            if number < pivot:
                low.append(number)
            else:
                high.append(number)
        return QuickSort(low) + [pivot] + QuickSort(high)

def main():
    numbers = sys.argv[1:]
    numbers = [int(number) for number in numbers]

    sorted_numbers = QuickSort(numbers)
    
    print_lista(sorted_numbers)

if __name__ == "__main__":
   main()
