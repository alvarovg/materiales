# Ordenación Lista de Enteros mediante algoritmo Quick Sort

Realiza un programa que ordene de menor a mayor una lista de números que se proporcionen como argumentos en la línea de comandos. Por ejemplo:

```commandline
python3 quick_sort.py 11 8 4 6 5
4 5 6 8 11 
```

Para realizar la ordenación, utiliza el método de ordenación por Quick Sort:

* Elegimos un elemento de la lista, llamado "pivote", que dividirá la lista en dos subconjuntos.
* Reorganizamos la lista de manera que todos los elementos menores que el pivote estén a su izquierda, y todos los elementos mayores estén a su derecha. Esto se llama "partición"
* Busca, entre los números que estén más a la derecha del lugar pivote, el lugar en que esté el número más pequeño. Llamamos a este lugar encontrado "lugar del menor"
* Recursivamente, aplicamos Quick Sort a los dos subconjuntos resultantes (uno a la izquierda del pivote y otro a la derecha). Repetimos este proceso para cada subconjunto hasta que la lista esté completamente ordenada

Solución: [quick_sort.py](quick_sort.py)