#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if firs is lower (alphabetically) than second, when lowercased

    `<` is only used on single characteres.
    """

    lower = False
    for pos in range(len(first)):
        if pos < len(second):
            if first[pos].lower() < second[pos].lower():
                lower = True
                break
            if first[pos].lower() > second[pos].lower():
                break
    return lower

def get_lower(words: list, pos:int):
    """Get lower word, for words right of pos (including pos)"""

    if pos == len(words)-1:
        return pos
    else:
        lower: int = pos
        for i in range(pos+1, len(words)):
            if is_lower(words[i], words[lower]):
                lower = i
        return lower

def sort(words: list):
    """Return the list of words, ordered alphabetically"""

    for pivot in range(len(words)):
        lower = get_lower(words, pivot)
        if lower != pivot:
            words[lower], words[pivot] = words[pivot], words[lower]
    return words

def show(words: list):

    for word in words:
        print(word, end=' ')
    print()

def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)

if __name__ == '__main__':
    main()
