#!/usr/bin/env pythoh3

"""Programa para ordenar cadenas que se reciben como argumentos (recursiva)"""

import sys

def order(l: list):
    if len(l) == 1:
        return l
    else:
        sublist = order(l[1:])
        for i in range(len(sublist)):
            if l[0] < sublist[i]:
                sublist.insert(i, l[0])
                return sublist
        sublist.append(l[0])
        return sublist

def main():
    if len(sys.argv) == 1:
        sys.exit("Hace falta una lista de argumentos")
    numbers: list = sys.argv[1:]
    ordered = order(numbers)
    print(ordered)

if __name__ == '__main__':
    main()
