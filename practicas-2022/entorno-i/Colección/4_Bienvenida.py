from datetime import datetime

nombre = input("Ingresa tu nombre: ")
fecha_nacimiento = input("Ingresa tu fecha de nacimiento (AAAA-MM-DD): ")

fecha_actual = datetime.now()
fecha_nacimiento = datetime.strptime(fecha_nacimiento, "%Y-%m-%d")
edad = fecha_actual.year - fecha_nacimiento.year - ((fecha_actual.month, fecha_actual.day) < (fecha_nacimiento.month, fecha_nacimiento.day))

print("¡Hola,", nombre, "! Tienes", edad, "años.")


